//
// Created by Ushakov Ilya on 2018-12-18.
//

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>
#include <stdio.h>


//#include "Client.h"

#include "Connection.cpp"

using namespace std;
class Client{
public:
    Client(int client_id, Connection connection) : client_id(client_id), connection(connection) {}
    void start(){

        //send client id
        send_client_id();

        int active = true;
        while(active){

            string description = "\nВыберите действие:\n"
                                 "1 - Создать новый заказ\n"
                                 "2 - Показать историю моих заказов\n"
                                 "3 - Показать историю заказов супермаркета\n"
                                 "4 - Добавить супермаркет\n";
            int action = get_new_action_int(description);

            switch(action){
                case 0: cancel(); active=false; break;
                case 1: if(!create_new_order()){active=false; } break;
                case 2: if(!show_order_history()){active=false;} break;
                case 3: if(!show_supermarket_history()){active=false;} break;
                case 4: if(!add_supermarket()){active=false;} break;
                default: puts("Нет такого действия");
            }

        }
    };


private:
    int client_id;
    Connection connection;
    string get_new_action(string description){
        char *result;
        puts(description.c_str());
        puts("Или 0 - Отключиться от сервера");

        scanf ("%[^\n]", result);
        printf ("Получили %s\n", result);
        return string(result);
    }

    bool send_client_id(){
        string server_response;
        connection.s(to_string(client_id), server_response);
        puts(server_response.c_str());
    }

    int get_new_action_int(string description){

        int result=-1;

        while(result < 0 || result > 100){
            puts(description.c_str()); puts("Или 0 - Отключиться от сервера");
            scanf("%d",&result);
            //cout << result << endl;
            while(getchar() != '\n');
        }

        printf ("Получили %d\n", result);
        return result;
    }
    bool create_new_order(){
        string request="1";
        int i_request;
        string server_response;

        //Получаем магазины
        if(!connection.s(request, server_response)) return false;


        //Выбираем магазин
        i_request = get_new_action_int(server_response); if(i_request==0) return false;
        if(!connection.s(i_request, server_response)) return false;



        //В цикле
        //Выбираем продукты и количество, отпралвяем продукты
        string products="";
        puts(server_response.c_str());

        int products_id, quantity;
            //Введите продукты или пустую строку, чтобы закончить выбор продуктов
            //Вводите продукты в формате <id продукта> <количество>
            //
            while(scanf("%d%d", &products_id, &quantity)!=0 || products.length()==0){
               products += to_string(products_id)+" "+to_string(quantity)+"\n";
               puts(server_response.c_str());
               puts("Уже выбрано:");
               puts(products.c_str());
               while(getchar() != '\n');
            }
            if(!connection.s(products, server_response)) return false;

        while(getchar() != '\n');

        //Пишем стоимость доставки, запрашиваем географичекую зону
        i_request = get_new_action_int(server_response); if(i_request==0) return false;
        if(!connection.s(i_request, server_response)) return false;

        puts(server_response.c_str());
        return true;
        //Закрываем заказ, возвращаем в меню

    }

    bool show_order_history(){
        string request="2";
        string server_response;

        //Получаем магазины
        if(!connection.s(request, server_response)) return false;
        puts(server_response.c_str());
        return true;
    }

    bool add_supermarket(){
        string request="4";
        int i_request;
        string server_response;
        //Инициируем добавление
        if(!connection.s(request, server_response)) return false;

        //Одним запросом вводим имя супермаркета
        request = get_new_action(server_response);
        if(!connection.s(request, server_response)) return false;

        // его удаленность
        //i_request = get_new_action_int(server_response); if(i_request==0) return false;
        //if(!connection.s(i_request, server_response)) return false;

        //В цикле добавляем продукты
        int product_price;
        char product_name[80];
        string products;

        puts(server_response.c_str());

        while(true){
            while(getchar() != '\n');
            puts("Название продукта или q, чтобы сохранить продукты:");
            scanf("%[^\n]", product_name);
            if(product_name[0]=='q') break;
            puts("Цена продукта:");
            scanf("%d", &product_price);
            products += string(product_name)+":"+to_string(product_price)+"\n";
        }
        //cout << "send to server products "<< products << endl;
        if(!connection.s(products, server_response)) return false;

        puts(server_response.c_str());
        return true;
    }
    bool show_supermarket_history(){
        string request="3";
        int i_request;
        string server_response;
        //Инициируем
        if(!connection.s(request, server_response)) return false;

        puts("Выберите магазин:");

        i_request = get_new_action_int(server_response); if(i_request==0) return false;
        if(!connection.s(i_request, server_response)) return false;

        puts(server_response.c_str());
        return true;

    }
    bool cancel(){
        string server_response;
        connection.sendRequest("0"); connection.cancel();
        return true;
    }


};