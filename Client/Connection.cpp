#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

using namespace std;
//#include "Connection.h"

class Connection {
private:
    int port;
    bool connected = false;
    string addr;
    int sock;


public:
    bool init(string _addr, int _port){
        port = _port;
        addr = _addr;
        struct sockaddr_in client;
        sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

        client.sin_family = AF_INET;
        client.sin_port = htons(port);
        client.sin_addr.s_addr = inet_addr(addr.c_str());
        int co = connect(sock, (struct sockaddr * )&client, sizeof(client));
        if (co < 0) {
            perror("Ошибка соединения");
            close(sock);
            return 0;
        }

        connected = true;
        return 1;
    }
    bool isConnected() {
        return connected;
    }
    bool sendRequest(string message){
        long sr = send(sock, message.c_str(), 512, 0);
        if (sr <= 0) { perror("Request error"); return false; }
        return true;
    }
    bool getResponse(string& response){
        char server_response[512];

        long sr = recv(sock, server_response, 512, 0);
        if (sr <= 0) { perror("Response error"); return false; } //TODO error handling
        response = string(server_response);
        return true;
    }
    bool s(string message, string& response){
        if(!sendRequest(message)) return false;
        if(!getResponse(response)) return false;
        return true;
    }
    bool s(int message, string& response){
        if(!sendRequest(to_string(message))) return false;
        if(!getResponse(response)) return false;
        return true;
    }
    void cancel(){
        shutdown(sock, SHUT_RDWR);
        close(sock);
    }
};