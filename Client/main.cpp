#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <unistd.h>

#include "Client.cpp"
#include <stdio.h>


using namespace std;

int main() {
//    char result[80];
//    int r;
//    int r1;
//
//    cout << scanf("%d%d", &r, &r1) << endl;
//    puts("Получили: ");
//    cout << r;
//    cout << r1;
//    return 0;

    int client_id=55;
    int port;
    string addr;
    Connection connection;

    if(!connection.init("127.0.0.1", 6707)){
        puts("Не удалось подключиться к серверу");
        return 0;
    }

    while(!connection.isConnected()){
        cout << "Enter server address:" << endl;
        cin >> addr;
        cout << "Enter server port:" << endl;
        cin >> port;
        connection.init(addr, port);
    }
    cout << "Connected to the server" << endl;

    if(client_id==0){
        cout << "Enter client id:" << endl;
        cin >> client_id;
    }
    Client client(client_id, connection);
    client.start();


    puts("Server disconnected you");
    connection.cancel();
    return 0;
}