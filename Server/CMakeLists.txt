cmake_minimum_required(VERSION 3.13)
project(Networks_Server)

set(CMAKE_CXX_STANDARD 14)

add_executable(Networks_Server main.cpp)