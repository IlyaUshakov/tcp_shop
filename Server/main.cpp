#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <pthread.h>
#include <iostream>
#include <mutex>
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <sys/stat.h>
#include <stdio.h>


#define maxClients 5
#define maxShops 150

using namespace std;

void *clientHandle(void *);
void *menuHandle(void *);
string getShops();


struct MyClient {
    int sock;
    sockaddr_in clientStr;
    int id;
    bool closed;
    int client_id;
    int shop_id;
    string action_status; //Какой статус этого действия
    string order;
    int delivery_zone;
};
class Shop {
    int shop_id;
public:
    void setName(string line){name = line;}
    void setShopId(int _shop_id){shop_id = _shop_id;}
    string getName(){ return name; }
    string showOrderHistory(){
        string filename = "../shops/"+to_string(shop_id)+"_orders.txt";

        ifstream ifs(filename);
        if(!ifs){
//            perror("Не могу найти файл заказаов магазина");
            return "Нет заказов";
        }
        string content( (std::istreambuf_iterator<char>(ifs) ),
                        (std::istreambuf_iterator<char>()    ) );

        return content;
    }
private:
    string name;
};
class Product{
    int product_id;
    int price;
    string name;
public:
    void init(int pid, string line){
        product_id = pid;
        string word;
        int iterator=0;
        stringstream stream(line);
        while( getline(stream, word, ':') ){
            if(iterator==0){ name = word; }
            if(iterator==1){ price = stoi(word); }
            iterator++;
        }
    }
    int getProductId(){ return product_id; }
    int getProductPrice(){ return price; }
    string getProductName(){ return name; }
    explicit operator std::string() const
    {
        return to_string(product_id+1)+" - "+ name+" за "+to_string(price)+"руб ";
    }

};

mutex m;
int number = 0;
MyClient clArray[maxClients];
Shop shops[maxShops];
pthread_t threadA[maxClients];


long readn(int as, char *buf, long n) {
    long sr = 0;
    while (n) {
        sr = recv(as, buf, n, 0);
        if (sr < 0) return -1;
        buf += sr;
        n -= sr;
    }
    return sr;
}

void showClients() {
    cout << "Сейчас подключено клиентов: " << number << endl;
    for(int i = 0; i < maxClients; i++) if (!clArray[i].closed)  cout << "Клиент № " << clArray[i].id << endl;
}

void closeClient(int key) {
    m.lock();
    number--;
    m.unlock();
    shutdown(clArray[key].sock, SHUT_RDWR);
    close(clArray[key].sock);
    pthread_join(threadA[key], NULL);
    m.lock();
    clArray[key].sock = -1;
    clArray[key].id = key;
    clArray[key].closed = true;
    m.unlock();
    cout << "Клиент № " << key << " отключен" << endl;
}

bool initShops(){
    //init shops
    ifstream infile("../shops.txt");
    if(!infile){
        perror("Не могу найти файл shops.txt");
        return 0;
    }
    string line;
    int i=0;
    while (getline(infile, line)) {
        shops[i].setName(line);
        shops[i].setShopId(i);
        if(i>=maxShops) break;
        i++;
    }
    return 1;
}
int main(int argc, char *argv[]) {

    if(initShops()==0) {
        perror("Нет файла с магазинами");
        return 1;
    }

    struct sockaddr_in server, client;
    int s = socket(AF_INET, SOCK_STREAM, 0);
    if (s < 0) {
        perror("Ошибка при создании сокета");
        exit(1);
    }
    puts("Сокет создан");
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = INADDR_ANY;
    server.sin_port = htons( 6707 );
    for (int i = 0; i < maxClients; i++) {
        clArray[i].action_status = "set_client_id";
        clArray[i].closed = true;
    }

    if( ::bind(s, (struct sockaddr *)&server, sizeof(server)) == -1){
        perror("Связь не установлена");
        return 1;
    }

    puts("Связь установлена");
    int ls = listen(s , 10);
    int c = sizeof(struct sockaddr_in);
    pthread_t threadMenu;
    pthread_create(&threadMenu, NULL, menuHandle, (socklen_t*)&s);
    while (true) {
        int as = -1;
        as = accept(s, (struct sockaddr *)&client, (socklen_t*)&c);
        int pos = 0;
        for (int i = 0; i < maxClients; i++) {
            if (clArray[i].closed) {
                pos = i;
                break;
            }
        }
        m.lock();
        clArray[pos].sock = as;
        clArray[pos].id = pos;
        clArray[pos].clientStr = client;
        clArray[pos].closed = false;


        m.unlock();
        if (clArray[pos].sock < 0) {
            perror("Ошибка принятия соединения");
            break;
        }
        else if (number + 1 == maxClients) {
            puts("Попытка подключения нового клиента заблокирована.");
            puts("Превышен лимит на подключения.");
            m.lock();
            number++;
            m.unlock();
            closeClient(pos);
            m.lock();
            clArray[pos].sock = -1;
            clArray[pos].closed = true;
            m.unlock();
        }
        else {
            cout << "Клиент № " << pos << " успешно подключен" << endl;
            ls = pthread_create(&threadA[number], NULL,  clientHandle, (void*)&pos);
            m.lock();
            number++;
            m.unlock();
        }
    }
    pthread_join(threadMenu, NULL);
    return 0;
}

void *menuHandle(void *socketServ) {
    int sock = *(int*)socketServ;
    for (;;) {
        puts("Выберите дальнейшие действия:");
        puts("1 - Отобразить всех клиентов");
        puts("2 - Отключить клиента");
        puts("0 - Выйти из сервера");
        int key;
        cin >> key;

        switch (key) {
            case 1: {
                showClients();
                break;
            }
            case 2: {
                puts("Наберите ID клиента для удаления");
                int key1;
                cin >> key1;
                closeClient(key1);
                break;
            }
            case 0: {
                puts("Выход из сервера. Все клиенты отключены.");
                for (int i = 0; i < maxClients; i++) {
                    shutdown(clArray[i].sock, SHUT_RDWR);
                    close(clArray[i].sock);
                }
                for (int i = 0; i < maxClients; i++) {
                    pthread_join(threadA[i], NULL);
                }
                shutdown(sock,2);
                close(sock);
                return 0;
            }
            default:{
                puts("Такого ключа нет");
            }
        }
        cout << endl;
    }
    return 0;
}
string getShops(){
    string result;
    for (int i = 0; i < maxShops; ++i)
    {
        if(shops[i].getName().length()) result += to_string(i+1) + " - " + shops[i].getName() + "\n";
    }
    return result;
}

string getShopProducts(int shopId){
    ifstream infile("../shops/"+to_string(shopId)+".txt");
    if(!infile){
        perror("Не могу найти файл shops/*.txt");
        return "";
    }
    string line;
    string result;
    int product_id = 0;
    while (getline(infile, line)) {
        Product product;
        product.init(product_id, line);
        result += "\n"+string(product);
        product_id++;
    }
    return result;
}
Product getProduct(int shopId, int product_id){
    ifstream infile("../shops/"+to_string(shopId)+".txt");
    if(!infile){
        perror("Не могу найти файл shops/*.txt");
    }
    string line;
    string result;
    int p_id = 0;
    Product product;
    while (getline(infile, line)) {

        product.init(p_id, line);
        result += "\n"+string(product);
        p_id++;
        if(product.getProductId()==product_id) return product;
    }
    return product;
}
bool setShopProducts(int shopId, string products){
    ofstream outfile;
    outfile.open("../shops/"+to_string(shopId-1)+".txt", std::ios_base::app);
    outfile << products;
    outfile.close();
    return true;
}
string getClientOrder(int client_key){
    int client_id = clArray[client_key].client_id;
    //TODO хранить в файле
    int shop_id = clArray[client_key].shop_id;
    string result;
    int price = 0;
    string line;

    stringstream stream(clArray[client_key].order);
    while (getline(stream, line)) {
        string word;
        int product_id;
        int product_quantity;
        string this_product;
        stringstream stream(line);
        while( getline(stream, word) ){
             product_id = stoi(word.substr(0, word.find(" ")));
             product_quantity = stoi(word.substr(word.find(" ")));
        }
        Product product = getProduct(shop_id, product_id);

        int product_price = product.getProductPrice();
        price += product_price*product_quantity;
        this_product =product.getProductName() + " "+ to_string(product_quantity)+" шт по "+to_string(product_price)+"\n";
//        cout << this_product;
        result += this_product;
    }
    return result+"Итого: "+to_string(price);
}
bool saveClientOrder(int client_key){

    int client_id = clArray[client_key].client_id;
    int shop_id = clArray[client_key].shop_id;
    //check if folder exists
    string order = "Заказ клиента #"+to_string(client_id)+" в магазине "+shops[shop_id].getName()+"\n"+getClientOrder(client_key);
    std::time_t result = std::time(nullptr);


    //add to clients
    string filename = "../clients/"+to_string(client_id)+"_orders.txt";

    ofstream f (filename, std::ios::app);
    f << endl << "______________" << endl;
    f << order;
    f.close();

    //add to shops

    filename = "../shops/"+to_string(shop_id)+"_orders.txt";

    ofstream f1 (filename, std::ios::app);
    f1 << endl << "______________" << endl;
    f1 << order;
    f1.close();



    return true;
}
// void addToMyOrder(int user, int shop_id, int product_id, int quantity){
// clArray[key].order += order+"\n";
// }

void addToMyOrder(int key, string order){
    clArray[key].order += order;
}
string getOrdersHistory(int client_id){
    string filename = "../clients/"+to_string(client_id)+"_orders.txt";
    ifstream ifs(filename);
    if(!ifs) return "У пользователя еще нет заказов";
    string content( (std::istreambuf_iterator<char>(ifs) ),
                         (std::istreambuf_iterator<char>()    ) );
    return content;
}

string handleClientActions(int key, char* message){
    string responce;
    if(message[0]=='0'){
        closeClient(key);
        return NULL;
    }
    if(clArray[key].action_status=="set_client_id"){
        int client_id = atoi(message);
        clArray[key].client_id = client_id;
        clArray[key].action_status="";
        return "Клиент "+to_string(client_id)+" успещно подключен";
    }
    if(clArray[key].action_status==""){ //Если действие не установлено
        if(message[0] == '1'){
            clArray[key].action_status = "select_shop";
            return "Создание нового заказа. \nВыберите магазин:\n"+
                   getShops();
        }
        if(message[0] == '2'){
            return getOrdersHistory(clArray[key].client_id);
        }
        if(message[0] == '3'){
            //История заказов супермаркета
            clArray[key].action_status = "supermarket_history";
            return getShops();
        }
        if(message[0] == '4'){ //Добавить супермаркет
            clArray[key].action_status = "add_supermarket";
            return "Введите название супермаркета";
        }

        return "Такого действия не установлено";
    }
    if(clArray[key].action_status == "supermarket_history"){
        clArray[key].action_status="";
        int shop_id = std::atoi(message)-1;

        return shops[shop_id].showOrderHistory();
    }
    if(clArray[key].action_status == "add_supermarket"){  clArray[key].action_status = "add_supermarket_products";
        int count = 1;
        string line;

        ifstream file("../shops.txt");
        while (getline(file, line))
            count++;
        file.close();

        clArray[key].shop_id = count;

        ofstream outfile;
        outfile.open("../shops.txt", std::ios_base::app);
        outfile << endl << message;
        outfile.close();
        return "Магазин создан, добавьте продукты";
    }
    if(clArray[key].action_status == "add_supermarket_products"){
        //cout << "new shop message: " << message << endl;
        setShopProducts(clArray[key].shop_id, string(message));
        initShops(); //Добавить созданные магазин для текущих подключений
        clArray[key].action_status="";
        return "Магазин создан, продукты добавлены";
    }

    if(clArray[key].action_status == "select_shop"){
        clArray[key].action_status = "choose_product";
        int shop_id = atoi(message)-1;
        clArray[key].shop_id = shop_id;
        return "Выбран магазин "+shops[shop_id].getName()+".\nВыберите товар:"+getShopProducts(shop_id)+"\nВ формате <Id товара> <Количество>. Или q, чтобы перейти к оформлению доставки";
    }
    if(clArray[key].action_status == "choose_product"){
            //Вывести сумму
            //ЗАпросить зону доствки
            addToMyOrder(key, string(message));
            clArray[key].action_status = "select_zone";
            return "Текущий заказ: \n"+getClientOrder(key)+"\nВведите свою зону доставки от 1 до 3х по отдаленности от магазина "+shops[clArray[key].shop_id].getName();
    }
    if(clArray[key].action_status == "select_zone"){
        clArray[key].action_status = "order_done";
        //Сохранить зону
        clArray[key].delivery_zone = message[0]- '0';
        int delivery_price = clArray[key].delivery_zone * 1000;
        saveClientOrder(key);
        clArray[key].order = "";
        clArray[key].action_status="";
        return "Доствка в вашу зону будет стоить "+to_string(delivery_price)+". Заказ оформлен. Ожадайте";
    }


    return "Нет такой операции";


}

void *clientHandle(void *socket){
    int p = *(int *) socket;
    char message[512];
    string responce;
    long sr = 0;
    while((sr = readn(clArray[p].sock, message, 512)) > 0) {
        if (sr <= 0) break;
        cout << "Клиент №" << clArray[p].id << " id="<<clArray[p].client_id <<" прислал: ";
        cout << message << endl;
        cout << "Статус клиента №" << clArray[p].id << ": " << clArray[p].action_status << endl;
        responce = handleClientActions(p, message);
        sr = send(clArray[p].sock, responce.c_str(), 512, 0);
    }
    return 0;
}
